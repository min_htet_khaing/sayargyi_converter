import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="nancy_converter",
    version="0.0.1",
    author="Min Htet Khaing",
    author_email="minhtetkhaing.dev@gmail.com",
    description="A converter package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/min_htet_khaing/sayargyi_converter.git",
    project_urls={
        "Bug Tracker": "https://gitlab.com/min_htet_khaing/sayargyi_converter/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
)